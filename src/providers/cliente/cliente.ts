import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';

/*
  Generated class for the ClienteProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json',
  })
};

@Injectable()

export class ClienteProvider {
  base_url:string

  constructor(public http: HttpClient) {
    console.log('Hello ClienteProvider Provider');
    this.base_url='http://127.0.0.1:8000/api'
  }
 
  crearcliente(data){
    return new Promise(resolve => {
      this.http.post(this.base_url+"/clientes",data, httpOptions)
          .subscribe(data => {
            resolve(data)
          })
    })
  }

  listarclientes(){
    return new Promise(resolve => {
      this.http.get(this.base_url+"/clientes", httpOptions)
          .subscribe(data => {
            resolve(data)
          })
    })
  }
}
