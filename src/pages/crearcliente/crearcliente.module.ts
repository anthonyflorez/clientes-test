import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CrearclientePage } from './crearcliente';
import { ClienteProvider } from '../../providers/cliente/cliente';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [
    CrearclientePage,
  ],
  imports: [
    IonicPageModule.forChild(CrearclientePage),
    HttpClientModule
  ],
  providers:[
    ClienteProvider
  ]
})
export class CrearclientePageModule {}
