import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { THROW_IF_NOT_FOUND } from '@angular/core/src/di/injector';
import { ClienteProvider } from '../../providers/cliente/cliente';
import { ClientesPage } from '../clientes/clientes';

/**
 * Generated class for the CrearclientePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-crearcliente',
  templateUrl: 'crearcliente.html',
})
export class CrearclientePage {
  clienteform:FormGroup;
  response:any

  constructor(public navCtrl: NavController, public navParams: NavParams, public alertCtrl:AlertController, public cliente:ClienteProvider,public formBuilder: FormBuilder) {
    this.clienteform = formBuilder.group({
      cedula: ['', Validators.required],
      nombre: ['', Validators.compose([Validators.required])],
      telefono: ['', Validators.compose([Validators.required])],
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CrearclientePage');
  }

  crearcliente(){
    this.cliente.crearcliente(this.clienteform.value).then(data=>{
      this.response = data
      if (this.response.response==true) {
        this.showError("Se agrego el cliente exitosamente","Exito")
        this.navCtrl.pop()
      }
    })
  }
  showError(text: string, title: string) {
    //this.loading.dismiss()

    let alert = this.alertCtrl.create({
      title: title,
      subTitle: text,
      buttons: ['OK']
    })
    alert.present(prompt)
  }

}
