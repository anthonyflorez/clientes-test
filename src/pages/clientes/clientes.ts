import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { CrearclientePage } from '../crearcliente/crearcliente';
import { ClienteProvider } from '../../providers/cliente/cliente';

/**
 * Generated class for the ClientesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-clientes',
  templateUrl: 'clientes.html',
})
export class ClientesPage {
  clientes:any
  response:any

  constructor(public navCtrl: NavController, public navParams: NavParams, public cliente:ClienteProvider) {


  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ClientesPage');
    
  }
  ionViewWillEnter(){
    console.log("data")
    this.cliente.listarclientes().then(data=>{
      this.response=data
      this.clientes=this.response.result
    })
  }

  gotoCreate(){
    this.navCtrl.push(CrearclientePage)
  }
}
